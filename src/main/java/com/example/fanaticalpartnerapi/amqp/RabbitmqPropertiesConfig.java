package com.example.fanaticalpartnerapi.amqp;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "amqp-rabbitmq")
public class RabbitmqPropertiesConfig {

    // PROCESSO NOVO USUARIO
    private String queueNewUser;
    private String exchangeNewUser;
    private String routingKeyNewUser;
    private String queueNewUserDlq;
    private String exchangeNewUserDlq;
    private String routingKeyNewUserDlq;
    private long queueNewUserMaxRetry;

    // PROCESSO DE ASSOCIACAO DE USUARIO COM CAMPANHAS
    private String queueRelateUserCampaign;
    private String exchangeRelateUserCampaign;
    private String routingKeyRelateUserCampaign;
    private String queueRelateUserCampaignDlq;
    private String exchangeRelateUserCampaignDlq;
    private String routingKeyRelateUserCampaignDlq;
    private long queueRelateUserCampaignMaxRetry;
}
