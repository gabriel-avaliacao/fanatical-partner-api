package com.example.fanaticalpartnerapi.amqp.publisher;

import com.example.fanaticalpartnerapi.amqp.RabbitmqPropertiesConfig;
import com.example.fanaticalpartnerapi.domain.UserEntity;
import com.example.fanaticalpartnerapi.dto.amqp.UserCampaignDTO;
import com.example.fanaticalpartnerapi.dto.amqp.UserMessageDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class NewUserPublisher {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private RabbitmqPropertiesConfig fanaticalProperties;

    public void publishNewUser(UserEntity userEntity){
        try{
            log.info("M=publishUserToRelate, status=start, mensagem enviada com sucesso, mensagem={}",userEntity);
            UserMessageDTO message = UserMessageDTO.builder()
                    .email(userEntity.getEmail())
                    .teamId(userEntity.getTeamEntity().getId())
                    .count(0L)
                    .build();
            rabbitTemplate.convertAndSend(fanaticalProperties.getExchangeNewUser(),
                    fanaticalProperties.getRoutingKeyNewUser(), message);
            log.info("M=publishUserToRelate, status=end, mensagem enviada com sucesso, mensagem={}",message);
        }catch (Exception e){
            log.info("M=publishUserToRelate, status=error, erro ao enviar mensagem para exchange={}, routing-key={}"
                    ,fanaticalProperties.getExchangeNewUser(),fanaticalProperties.getRoutingKeyNewUser());
            throw e;
        }
    }

    public void publishRelateUserCampaign(UserCampaignDTO userCampaignDTO) {
        try{
            log.info("M=publishRelateUserCampaign, status=start, mensagem enviada com sucesso, mensagem={}",userCampaignDTO);
            rabbitTemplate.convertAndSend(fanaticalProperties.getExchangeRelateUserCampaign(),
                    fanaticalProperties.getRoutingKeyRelateUserCampaign(), userCampaignDTO);
            log.info("M=publishRelateUserCampaign, status=end, mensagem enviada com sucesso, mensagem={}",userCampaignDTO);
        }catch (Exception e){
            log.info("M=publishRelateUserCampaign, status=error, erro ao enviar mensagem para exchange={}, routing-key={}"
                    ,fanaticalProperties.getExchangeRelateUserCampaign(),fanaticalProperties.getRoutingKeyRelateUserCampaign());
            throw e;
        }
    }
}
