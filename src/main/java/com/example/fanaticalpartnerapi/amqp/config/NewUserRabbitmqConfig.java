package com.example.fanaticalpartnerapi.amqp.config;

import com.example.fanaticalpartnerapi.amqp.RabbitmqPropertiesConfig;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class NewUserRabbitmqConfig {

    @Autowired
    private RabbitmqPropertiesConfig propertiesConfig;

    @Bean
    public Queue queueNewUser() {
        Map<String, Object> arguments = new HashMap<>();
        arguments.put("x-dead-letter-exchange", propertiesConfig.getExchangeNewUserDlq());
        arguments.put("x-dead-letter-routing-key", propertiesConfig.getRoutingKeyNewUserDlq());
        return new Queue(propertiesConfig.getQueueNewUser(), true,false,false, arguments);
    }

    @Bean
    public Queue queueNewUserDlq() {
        return new Queue(propertiesConfig.getQueueNewUserDlq(), true);
    }

    @Bean
    public DirectExchange exchangeNewUser() {
        return new DirectExchange(propertiesConfig.getExchangeNewUser());
    }

    @Bean
    public DirectExchange exchangeNewUserDlq() {
        return new DirectExchange(propertiesConfig.getExchangeNewUserDlq());
    }

    @Bean
    public Binding bindingNewUser() {
        return BindingBuilder.bind(queueNewUser()).to(exchangeNewUser())
                .with(propertiesConfig.getRoutingKeyNewUser());
    }

    @Bean
    public Binding bindingNewUserDlq() {
        return BindingBuilder.bind(queueNewUserDlq()).to(exchangeNewUserDlq())
                .with(propertiesConfig.getRoutingKeyNewUserDlq());
    }

}
