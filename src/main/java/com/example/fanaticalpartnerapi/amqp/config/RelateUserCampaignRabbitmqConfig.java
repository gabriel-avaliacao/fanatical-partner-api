package com.example.fanaticalpartnerapi.amqp.config;

import com.example.fanaticalpartnerapi.amqp.RabbitmqPropertiesConfig;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class RelateUserCampaignRabbitmqConfig {

    @Autowired
    private RabbitmqPropertiesConfig propertiesConfig;

    @Bean
    public Queue queueRelateUserCampaign() {
        Map<String, Object> arguments = new HashMap<>();
        arguments.put("x-dead-letter-exchange", propertiesConfig.getExchangeRelateUserCampaignDlq());
        arguments.put("x-dead-letter-routing-key", propertiesConfig.getRoutingKeyRelateUserCampaignDlq());
        return new Queue(propertiesConfig.getQueueRelateUserCampaign(), true,false,false, arguments);
    }

    @Bean
    public Queue queueRelateUserCampaignDlq() {
        return new Queue(propertiesConfig.getQueueRelateUserCampaignDlq(), true);
    }

    @Bean
    public DirectExchange exchangeRelateUserCampaign() {
        return new DirectExchange(propertiesConfig.getExchangeRelateUserCampaign());
    }

    @Bean
    public DirectExchange exchangeRelateUserCampaignDlq() {
        return new DirectExchange(propertiesConfig.getExchangeRelateUserCampaignDlq());
    }

    @Bean
    public Binding bindingRelateUserCampaign() {
        return BindingBuilder.bind(queueRelateUserCampaign()).to(exchangeRelateUserCampaign())
                .with(propertiesConfig.getRoutingKeyRelateUserCampaign());
    }

    @Bean
    public Binding bindingRelateUserCampaignDlq() {
        return BindingBuilder.bind(queueRelateUserCampaignDlq()).to(exchangeRelateUserCampaignDlq())
                .with(propertiesConfig.getRoutingKeyRelateUserCampaignDlq());
    }

}
