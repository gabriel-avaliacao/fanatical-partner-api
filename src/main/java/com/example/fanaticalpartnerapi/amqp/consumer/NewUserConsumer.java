package com.example.fanaticalpartnerapi.amqp.consumer;

import com.example.fanaticalpartnerapi.amqp.RabbitmqPropertiesConfig;
import com.example.fanaticalpartnerapi.amqp.publisher.GenericPublisher;
import com.example.fanaticalpartnerapi.amqp.publisher.NewUserPublisher;
import com.example.fanaticalpartnerapi.dto.CampaignDTO;
import com.example.fanaticalpartnerapi.dto.amqp.UserCampaignDTO;
import com.example.fanaticalpartnerapi.dto.amqp.UserMessageDTO;
import com.example.fanaticalpartnerapi.service.CampaignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class NewUserConsumer {

    public static final String QUEUE_NEW_USER = "queue_new_user";

    @Autowired
    private CampaignService campaignService;

    @Autowired
    private RabbitmqPropertiesConfig fanaticalProperties;

    @Autowired
    private NewUserPublisher newUserPublisher;

    @Autowired
    private GenericPublisher genericPublisher;

    @RabbitListener(queues = QUEUE_NEW_USER)
    public void consumerNewUser(UserMessageDTO messageDTO){
        try{
            log.info("M=consumerNewUser, status=start, buscando campanhas ativas para vinculo com usuario, mensagem={}",messageDTO);
            isValidMessage(messageDTO);
            List<CampaignDTO> campaignDTOS = campaignService.findCampaignActivesByTeam(messageDTO.getTeamId());
            if(hasCampaign(campaignDTOS)){
                campaignDTOS.stream()
                         .map(c -> UserCampaignDTO.builder().email(messageDTO.getEmail()).campaignId(c.getId()).build())
                         .forEach(this::sendMessageRelateUserCampaign);
            }
        } catch (AmqpRejectAndDontRequeueException e) {
            log.info("M=consumerNewUser, status=AmqpRejectAndDontRequeueException, buscando campanhas ativas para vinculo com usuario, mensagem={}",messageDTO);
            throw e;
        } catch(Exception e){
            log.info("M=consumerNewUser, status=error-generic, buscando campanhas ativas para vinculo com usuario, mensagem={}",messageDTO);
            messageDTO.plusCount();
            genericPublisher.publishRetry(messageDTO,fanaticalProperties.getExchangeNewUser(),
                    fanaticalProperties.getRoutingKeyNewUser());
        }
    }

    private void sendMessageRelateUserCampaign(UserCampaignDTO userCampaignDTO) {
        newUserPublisher.publishRelateUserCampaign(userCampaignDTO);
    }

    private boolean hasCampaign(List<CampaignDTO> campaignActivesByTeam) {
        return campaignActivesByTeam != null && !campaignActivesByTeam.isEmpty();
    }

    private void isValidMessage(UserMessageDTO messageDTO) {
        if(messageDTO.toRetry(fanaticalProperties.getQueueNewUserMaxRetry())){
            throw new AmqpRejectAndDontRequeueException
                    ("Máximo de tentativas atingido! da mensagem="+ messageDTO.toString());
        }
    }

}
