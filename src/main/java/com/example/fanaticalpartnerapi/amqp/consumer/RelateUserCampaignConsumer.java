package com.example.fanaticalpartnerapi.amqp.consumer;

import com.example.fanaticalpartnerapi.amqp.RabbitmqPropertiesConfig;
import com.example.fanaticalpartnerapi.amqp.publisher.GenericPublisher;
import com.example.fanaticalpartnerapi.dto.amqp.UserCampaignDTO;
import com.example.fanaticalpartnerapi.service.CampaignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RelateUserCampaignConsumer {

    private static final String QUEUE_RELATE_USER_CAMPAIGN = "queue_relate_user_campaign";

    @Autowired
    private CampaignService campaignService;

    @Autowired
    private GenericPublisher genericPublisher;

    @Autowired
    private RabbitmqPropertiesConfig fanaticalProperties;

    @RabbitListener(queues = QUEUE_RELATE_USER_CAMPAIGN)
    public void consumerRelateUserCampaign(UserCampaignDTO message){
        try{
            log.info("M=consumerRelateUserCampaign, status=start, associando usuário a campanha, mensagem={}",message);
            isValidMessage(message);
            campaignService.relateUserCampaign(message);
        } catch (AmqpRejectAndDontRequeueException e) {
            log.info("M=consumerRelateUserCampaign, status=AmqpRejectAndDontRequeueException, buscando campanhas ativas para vinculo com usuario, mensagem={}",message);
            throw e;
        } catch(Exception e){
            log.info("M=consumerRelateUserCampaign, status=error-generic, buscando campanhas ativas para vinculo com usuario, mensagem={}",message);
            message.plusCount();
            genericPublisher.publishRetry(message,fanaticalProperties.getExchangeRelateUserCampaign(),
                    fanaticalProperties.getRoutingKeyRelateUserCampaign());
        }
    }

    private void isValidMessage(UserCampaignDTO message) {
        if(message.toRetry(fanaticalProperties.getQueueRelateUserCampaignMaxRetry())){
            throw new AmqpRejectAndDontRequeueException
                    ("Máximo de tentativas atingido! da mensagem="+ message.toString());
        }
    }
}
