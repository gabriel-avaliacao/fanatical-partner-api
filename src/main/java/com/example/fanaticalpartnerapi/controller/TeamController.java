package com.example.fanaticalpartnerapi.controller;

import com.example.fanaticalpartnerapi.controller.request.TeamRequest;
import com.example.fanaticalpartnerapi.controller.response.TeamResponse;
import com.example.fanaticalpartnerapi.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("team")
public class TeamController {

    @Autowired
    private TeamService teamService;

    @PostMapping
    public ResponseEntity<TeamResponse> create(@Valid @RequestBody TeamRequest teamRequest){
        return new ResponseEntity(teamService.save(teamRequest), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id){
        teamService.delete(id);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    @GetMapping
    public ResponseEntity<List<TeamResponse>> findsAll(){
        return new ResponseEntity(teamService.findsAll(),HttpStatus.OK);
    }
}
