package com.example.fanaticalpartnerapi.controller;

import com.example.fanaticalpartnerapi.controller.request.UserRequest;
import com.example.fanaticalpartnerapi.controller.response.UserResponse;
import com.example.fanaticalpartnerapi.dto.CampaignDTO;
import com.example.fanaticalpartnerapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public ResponseEntity<UserResponse> register(@Valid @RequestBody UserRequest userRequest){
        return new ResponseEntity(userService.saveUser(userRequest), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<CampaignDTO>> checkUser(@RequestParam("email") String email){
        return new ResponseEntity(userService.checkUser(email), HttpStatus.OK);
    }
}
