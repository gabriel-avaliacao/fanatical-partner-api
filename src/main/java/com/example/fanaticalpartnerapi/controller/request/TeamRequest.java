package com.example.fanaticalpartnerapi.controller.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class TeamRequest {

    @NotNull
    @NotBlank
    private String name;

}
