package com.example.fanaticalpartnerapi.controller.request;

import com.example.fanaticalpartnerapi.utils.ConstatsValues;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class UserRequest {

    @NotNull
    @NotBlank
    private String email;

    private String name;

    @ApiModelProperty(example = "00/00/0000")
    @Pattern(regexp = ConstatsValues.PATTERN_CAMPAIGN_PERIOD_REGEX,
            message = ConstatsValues.PATTERN_CAMPAIGN_PERIOD_REGEX_MESSAGE_ERROR)
    private String birthday;

    @NotNull
    private Long teamId;
}
