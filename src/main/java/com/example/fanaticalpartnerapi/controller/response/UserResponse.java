package com.example.fanaticalpartnerapi.controller.response;

import com.example.fanaticalpartnerapi.utils.ConstatsValues;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class UserResponse {

    private String email;

    private String name;

    @JsonFormat(pattern = ConstatsValues.PATTERN_CAMPAIGN_PERIOD_FORMAT)
    private LocalDate birthday;

    private TeamResponse team;
}
