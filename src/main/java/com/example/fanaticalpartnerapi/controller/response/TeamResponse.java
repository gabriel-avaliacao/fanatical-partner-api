package com.example.fanaticalpartnerapi.controller.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TeamResponse {

    private Long id;
    private String name;

}
