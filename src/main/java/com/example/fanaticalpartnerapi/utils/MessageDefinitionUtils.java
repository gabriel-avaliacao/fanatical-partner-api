package com.example.fanaticalpartnerapi.utils;


import com.example.fanaticalpartnerapi.controller.response.DefaultErrorResponse;

public enum MessageDefinitionUtils {

    ERROR_DATE_VALIDATE("100000","Verifique a data informada."),
    ERROR_TEAM_NOT_FOUND("100001","Time do coração não encontrado!"),
    ERROR_TEAM_CANNOT_BE_DELETED("100002","Time do coração não pode ser deletado pois possui vinculo com algum usuários!"),
    ERROR_CAMPAIGN_API_UNKNOW("100003","Erro ao consultar campanhas ativas"),
    ERROR_CAMPAIGN_API_NOT_FOUND_ACTIVES("100004","Nenhuma campanha ativa encontrada"),
    ERROR_EMAIL_ALREADY_REGISTERED("100005","Cadastro já foi efetuado! Favor usar outro email."),
    ERROR_USER_NOT_FOUND("100006","Usuário não encontrado com o email informado."),
    ;
    private final String code;
    private final String description;

    MessageDefinitionUtils(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public DefaultErrorResponse getDefaultErrorResponse(){
        return DefaultErrorResponse.builder()
                .code(code)
                .description(description)
                .build();
    }

}
