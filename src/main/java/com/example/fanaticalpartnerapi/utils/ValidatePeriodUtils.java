package com.example.fanaticalpartnerapi.utils;

import com.example.fanaticalpartnerapi.exception.ValidateDateException;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Slf4j
public class ValidatePeriodUtils {

    private ValidatePeriodUtils(){}

    public static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    public static LocalDate isValid(String dateString){
        try{
            return LocalDate.parse(dateString,DATE_FORMAT);
        }catch (Exception e){
            log.error("M=isValid, error to parsing date,  date={}",dateString);
            throw new ValidateDateException(MessageDefinitionUtils.ERROR_DATE_VALIDATE);
        }
    }

}
