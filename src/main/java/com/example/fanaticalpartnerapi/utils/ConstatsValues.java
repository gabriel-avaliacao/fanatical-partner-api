package com.example.fanaticalpartnerapi.utils;

public class ConstatsValues {

    private ConstatsValues(){}

    public static final String PATTERN_CAMPAIGN_PERIOD_FORMAT = "dd/MM/yyyy";
    public static final String PATTERN_CAMPAIGN_PERIOD_REGEX = "^[0-9]{2}\\/[0-9]{2}\\/[0-9]{4}$";
    public static final String PATTERN_CAMPAIGN_PERIOD_REGEX_MESSAGE_ERROR = "Format is not the same as this  00/00/0000";

}