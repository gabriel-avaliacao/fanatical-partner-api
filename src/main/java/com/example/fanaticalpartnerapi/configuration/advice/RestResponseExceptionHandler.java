package com.example.fanaticalpartnerapi.configuration.advice;

import com.example.fanaticalpartnerapi.controller.response.DefaultErrorResponse;
import com.example.fanaticalpartnerapi.exception.FanaticalPartnerException;
import com.example.fanaticalpartnerapi.exception.ValidateDateException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class RestResponseExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Map<String, String>> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<DefaultErrorResponse> handleDataIntegrityViolationException(DataIntegrityViolationException e) {
        DefaultErrorResponse.DefaultErrorResponseBuilder builder = DefaultErrorResponse.builder();
        if(e.getCause().getCause()!= null && !ObjectUtils.isEmpty(e.getCause().getCause().getMessage())){
            builder.description(e.getCause().getCause().getMessage());
        } else {
            builder.description(e.getCause().getMessage());
        }
        return new ResponseEntity(builder.build(),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ValidateDateException.class)
    public ResponseEntity<DefaultErrorResponse> handleValidateDateException(ValidateDateException exception) {
        return new ResponseEntity(exception.getMessageDefinition().getDefaultErrorResponse(),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(FanaticalPartnerException.class)
    public ResponseEntity<DefaultErrorResponse> handleFanaticalPartnerException(FanaticalPartnerException exception) {
        return new ResponseEntity(exception.getMessageDefinition().getDefaultErrorResponse(),
                HttpStatus.BAD_REQUEST);
    }
}
