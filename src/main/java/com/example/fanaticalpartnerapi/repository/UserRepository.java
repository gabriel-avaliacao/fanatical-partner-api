package com.example.fanaticalpartnerapi.repository;

import com.example.fanaticalpartnerapi.domain.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, String> {

    long countByTeamEntityId(Long teamId);
}
