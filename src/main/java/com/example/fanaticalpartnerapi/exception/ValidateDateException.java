package com.example.fanaticalpartnerapi.exception;

import com.example.fanaticalpartnerapi.utils.MessageDefinitionUtils;
import lombok.Getter;

public class ValidateDateException extends RuntimeException {

    @Getter
    private MessageDefinitionUtils messageDefinition;

    public ValidateDateException(MessageDefinitionUtils messageDefinition) {
        this.messageDefinition = messageDefinition;
    }
}
