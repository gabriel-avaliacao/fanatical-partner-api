package com.example.fanaticalpartnerapi.exception;

import com.example.fanaticalpartnerapi.utils.MessageDefinitionUtils;
import lombok.Getter;

public class FanaticalPartnerException extends RuntimeException {

    @Getter
    private MessageDefinitionUtils messageDefinition;

    public FanaticalPartnerException() {
    }

    public FanaticalPartnerException(MessageDefinitionUtils messageDefinition) {
        this.messageDefinition = messageDefinition;
    }
}
