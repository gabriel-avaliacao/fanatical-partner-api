package com.example.fanaticalpartnerapi.service;

import com.example.fanaticalpartnerapi.dto.CampaignDTO;
import com.example.fanaticalpartnerapi.dto.amqp.UserCampaignDTO;

import java.util.List;

public interface CampaignService {

    List<CampaignDTO> findCampaignActivesByTeam(Long teamId);

    void relateUserCampaign(UserCampaignDTO userCampaignDTO);

    List<CampaignDTO> findCampaignByUser(String email);
}
