package com.example.fanaticalpartnerapi.service.impl;

import com.example.fanaticalpartnerapi.controller.request.TeamRequest;
import com.example.fanaticalpartnerapi.controller.response.TeamResponse;
import com.example.fanaticalpartnerapi.domain.TeamEntity;
import com.example.fanaticalpartnerapi.exception.FanaticalPartnerException;
import com.example.fanaticalpartnerapi.repository.TeamRepository;
import com.example.fanaticalpartnerapi.repository.UserRepository;
import com.example.fanaticalpartnerapi.service.TeamService;
import com.example.fanaticalpartnerapi.utils.MessageDefinitionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeamServiceImpl implements TeamService {

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private UserRepository userRepository;

    /**
     * Busca uma entidade TeamEntity pelo identificador
     * @param teamId identificador
     * @return TeamEntity Objeto que representa uma entidade do time
     */
    @Override
    public TeamEntity findById(Long teamId) {
        return teamRepository.findById(teamId).orElseThrow(() ->
                new FanaticalPartnerException(MessageDefinitionUtils.ERROR_TEAM_NOT_FOUND));
    }

    /**
     * Converte uma entidade para um objeto TeamResponse
     * @param teamEntity Objeto que representa uma entidade do time
     * @return TeamResponse Objeto de resposta que representa um time
     */
    @Override
    public TeamResponse convertToResponse(TeamEntity teamEntity) {
        return TeamResponse.builder()
                .id(teamEntity.getId())
                .name(teamEntity.getName())
                .build();
    }


    /**
     * Salva uma time na base de dados
     * @param teamRequest Objeto de requisição que representa um time
     * @return TeamResponse Objeto de resposta que representa um time
     */
    @Override
    public TeamResponse save(TeamRequest teamRequest) {
        TeamEntity teamEntity = teamRepository.save(convertToEntity(teamRequest));
        return convertToResponse(teamEntity);
    }

    /**
     * Delete um time através do identificador
     * @param teamId identificador do time
     */
    @Override
    public void delete(Long teamId) {
        findById(teamId);
        checkUserLinkWithTeam(teamId);
        teamRepository.deleteById(teamId);
    }

    /**
     * Busca todos os times cadastrados na base de dados
     * @return List<TeamResponse> lista de representações de um time
     */
    @Override
    public List<TeamResponse> findsAll() {
        List<TeamEntity> teamEntities = teamRepository.findAll();
        return teamEntities.stream().map(this::convertToResponse).collect(Collectors.toList());
    }

    private void checkUserLinkWithTeam(Long teamId) {
        long l = countUserWithTeam(teamId);
        if(l > 0) throw new FanaticalPartnerException(MessageDefinitionUtils.ERROR_TEAM_CANNOT_BE_DELETED);
    }

    private long countUserWithTeam(Long teamId) {
        return userRepository.countByTeamEntityId(teamId);
    }

    private TeamEntity convertToEntity(TeamRequest teamRequest) {
        TeamEntity teamEntity = new TeamEntity();
        teamEntity.setName(teamRequest.getName());
        return teamEntity;
    }
}
