package com.example.fanaticalpartnerapi.service.impl;

import com.example.fanaticalpartnerapi.amqp.publisher.NewUserPublisher;
import com.example.fanaticalpartnerapi.controller.request.UserRequest;
import com.example.fanaticalpartnerapi.controller.response.UserResponse;
import com.example.fanaticalpartnerapi.domain.TeamEntity;
import com.example.fanaticalpartnerapi.domain.UserEntity;
import com.example.fanaticalpartnerapi.dto.CampaignDTO;
import com.example.fanaticalpartnerapi.dto.amqp.UserCampaignDTO;
import com.example.fanaticalpartnerapi.exception.FanaticalPartnerException;
import com.example.fanaticalpartnerapi.repository.UserRepository;
import com.example.fanaticalpartnerapi.service.CampaignService;
import com.example.fanaticalpartnerapi.service.TeamService;
import com.example.fanaticalpartnerapi.service.UserService;
import com.example.fanaticalpartnerapi.utils.MessageDefinitionUtils;
import com.example.fanaticalpartnerapi.utils.ValidatePeriodUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TeamService teamService;

    @Autowired
    private NewUserPublisher newUserPublisher;

    @Autowired
    private CampaignService campaignService;

    /**
     * Salva na base de dados um novo usuário.
     * Caso exista um usuário com o mesmo email informado uma mensagem informando isso
     * será devolvida.
     * @param userRequest representação de um objeto de requisição usuário
     * @return UserResponse representação de uma entidade usuário
     */
    @Override
    public UserResponse saveUser(UserRequest userRequest) {
        checkIfUserExists(userRequest.getEmail());
        UserEntity userEntity = save(convertToEntity(userRequest));
        publisherNewUser(userEntity);
        return convertToResponse(userEntity);
    }

    private void checkIfUserExists(String email) {
        Optional<UserEntity> optionalUserEntity = userRepository.findById(email);
        if(optionalUserEntity.isPresent()) throw new FanaticalPartnerException(MessageDefinitionUtils.ERROR_EMAIL_ALREADY_REGISTERED);
    }

    /**
     * Verifica se o usuário existe na base dade dados, caso não exista uma mensagem de erro
     * será devolvida lhe informando isso.
     * Caso exista uma chamada no serviço Campaing API é feita
     * buscando as campanhas vinculadas a ele, se existir campanhas uma mensagem informando que o
     * cadastro já foi efetuado será devolvida.
     * Caso o usuário não possua nenhuma campanha vinculada uma chamada no serviço Campaign API é feita
     * buscando todas as campanhas ativas para o time do coração e serão retornadas para o usuário,
     * em paralelo essas campanhas serão associadas ao usuário.
     * @param email identificador do usuário
     * @return List<CampaignDTO> Lista de representações de campanhas
     */
    @Override
    public List<CampaignDTO> checkUser(String email) {
        Optional<UserEntity> optionalUserEntity = userRepository.findById(email);
        List<CampaignDTO> campaignByUser = campaignService.findCampaignByUser(email);
        if(optionalUserEntity.isPresent() && !campaignByUser.isEmpty()){
            throw new FanaticalPartnerException(MessageDefinitionUtils.ERROR_EMAIL_ALREADY_REGISTERED);
        } else if(optionalUserEntity.isPresent()){
            List<CampaignDTO> campaignDTOList = campaignService
                    .findCampaignActivesByTeam(optionalUserEntity.get().getTeamEntity().getId());
            if(!campaignDTOList.isEmpty()){
                publishNewCampaignsToUser(email, campaignDTOList);
                return campaignDTOList;
            }else{
                throw new FanaticalPartnerException(MessageDefinitionUtils.ERROR_CAMPAIGN_API_NOT_FOUND_ACTIVES);
            }
        } else {
            throw new FanaticalPartnerException(MessageDefinitionUtils.ERROR_USER_NOT_FOUND);
        }
    }

    private void publishNewCampaignsToUser(String email, List<CampaignDTO> campaignDTOList) {
        try{
            campaignDTOList.forEach(c -> newUserPublisher.publishRelateUserCampaign(
                    UserCampaignDTO.builder().campaignId(c.getId()).email(email).build()));
        }catch (Exception e){
            log.error("M=checkUser, status=error, erro ao publicar mensagens de relacionamento com campanhas, email={}", email);
        }
    }

    private void publisherNewUser(UserEntity userEntity) {
        newUserPublisher.publishNewUser(userEntity);
    }

    private UserResponse convertToResponse(UserEntity userEntity) {
        return UserResponse.builder()
                .email(userEntity.getEmail())
                .name(userEntity.getName())
                .birthday(userEntity.getBirthday())
                .team(teamService.convertToResponse(userEntity.getTeamEntity()))
                .build();
    }

    private UserEntity save(UserEntity userEntity) {
        return userRepository.save(userEntity);
    }

    private UserEntity convertToEntity(UserRequest userRequest) {
        UserEntity userEntity = new UserEntity();
        userEntity.setEmail(userRequest.getEmail());
        userEntity.setBirthday(ValidatePeriodUtils.isValid(userRequest.getBirthday()));
        userEntity.setName(userRequest.getName());
        userEntity.setTeamEntity(findTeamById(userRequest.getTeamId()));
        return userEntity;
    }

    private TeamEntity findTeamById(Long teamId) {
        return teamService.findById(teamId);
    }
}
