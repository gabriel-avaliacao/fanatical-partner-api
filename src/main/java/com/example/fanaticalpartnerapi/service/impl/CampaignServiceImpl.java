package com.example.fanaticalpartnerapi.service.impl;

import com.example.fanaticalpartnerapi.dto.CampaignDTO;
import com.example.fanaticalpartnerapi.dto.amqp.UserCampaignDTO;
import com.example.fanaticalpartnerapi.exception.FanaticalPartnerException;
import com.example.fanaticalpartnerapi.service.CampaignClient;
import com.example.fanaticalpartnerapi.service.CampaignService;
import com.example.fanaticalpartnerapi.utils.MessageDefinitionUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CampaignServiceImpl implements CampaignService {

    @Autowired
    private CampaignClient campaignClient;

    /**
     * Realiza uma chamada no serviço Campaign API buscando campanhas ativas vinculadas com um time
     * @param teamId Identificador de uma time
     * @return List<CampaignDTO> Lista de representações de uma campanha
     */
    @Override
    public List<CampaignDTO> findCampaignActivesByTeam(Long teamId){
        try{
            log.info("M=findCampaignActivesByTeam, status=start teamId={}",teamId);
            CampaignDTO[] reponseCampaigns = campaignClient.findCampaignByTeam(teamId);
            if(reponseCampaigns != null && reponseCampaigns.length != 0){
                return Arrays.stream(reponseCampaigns).collect(Collectors.toList());
            }else {
                log.info("M=findCampaignActivesByTeam, status=end, Nenhuma campanha ativa encontrada.");
                return Collections.emptyList();
            }
        }catch (Exception e){
            log.error("M=findCampaignActivesByTeam, status=error, Erro ao consultar campanhas ativas, error={}",e.getMessage());
            throw new FanaticalPartnerException(MessageDefinitionUtils.ERROR_CAMPAIGN_API_UNKNOW);
        }
    }

    /**
     * Realiza uma chamada no serviço Campaign API relacionando um usuário com uma campanha
     * @param userCampaignDTO Objeto contendo dados do usuário e campanha
     */
    @Override
    public void relateUserCampaign(UserCampaignDTO userCampaignDTO){
        try{
            log.info("M=relateUserCampaign, status=start, chamando serviço campaign-api para relacionar " +
                    "  campanha com usuário, userCampaignDTO={}",userCampaignDTO);

            ResponseEntity<String> response = campaignClient.relateUserCampaign(userCampaignDTO.getCampaignId(),
                    userCampaignDTO.getEmail());
            if(!response.getStatusCode().is2xxSuccessful()){
                log.info("M=relateUserCampaign, status=error, Nenhuma campanha ativa encontrada.");
                throw new FanaticalPartnerException(MessageDefinitionUtils.ERROR_CAMPAIGN_API_NOT_FOUND_ACTIVES);
            }
        }catch (Exception e){
            log.error("M=relateUserCampaign, status=error, Erro ao consultar campanhas ativas, error={}",e.getMessage());
            throw e;
        }
    }

    /**
     * Realiza uma chamada no serviço Campaign API buscando campanhas relacionadas com um
     * usuário.
     * @param email identificador do usuário
     * @return List<CampaignDTO> Lista de representações de uma campanha
     */
    @Override
    public List<CampaignDTO> findCampaignByUser(String email) {
        try{
            log.info("M=findCampaignByUser, status=start email={}",email);

            CampaignDTO[] reponseCompaigns = campaignClient.findCampaignByUser(email);

            if(reponseCompaigns != null && reponseCompaigns.length != 0){
                return Arrays.stream(reponseCompaigns).collect(Collectors.toList());
            }
            log.info("M=findCampaignByUser, status=end, nenhuma campanha vinculada com o usuário, email={}",email);
            return Collections.emptyList();
        }catch (Exception e){
            log.error("M=findCampaignByUser, status=error, Erro ao consultar campanhas ativas, error={}",e.getMessage());
            throw new FanaticalPartnerException(MessageDefinitionUtils.ERROR_CAMPAIGN_API_UNKNOW);
        }
    }

}
