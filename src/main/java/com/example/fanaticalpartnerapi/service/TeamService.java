package com.example.fanaticalpartnerapi.service;

import com.example.fanaticalpartnerapi.controller.request.TeamRequest;
import com.example.fanaticalpartnerapi.controller.response.TeamResponse;
import com.example.fanaticalpartnerapi.domain.TeamEntity;

import java.util.List;

public interface TeamService {

    TeamEntity findById(Long teamId);

    TeamResponse convertToResponse(TeamEntity teamEntity);

    TeamResponse save(TeamRequest teamRequest);

    void delete(Long teamId);

    List<TeamResponse> findsAll();
}
