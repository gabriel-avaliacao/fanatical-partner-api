package com.example.fanaticalpartnerapi.service;

import com.example.fanaticalpartnerapi.dto.CampaignDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "campaign-api", path = "/campaign")
public interface CampaignClient {

    @GetMapping(value = "/team/{id}")
    CampaignDTO[] findCampaignByTeam(@PathVariable("id") Long id);

    @GetMapping(value = "/find/customer")
    CampaignDTO[] findCampaignByUser(@RequestParam("email") String email);

    @PostMapping(value = "/{campaignId}/relate/customer")
    ResponseEntity<String> relateUserCampaign(@PathVariable("campaignId") Long campaignId, @RequestParam("email") String email);
}
