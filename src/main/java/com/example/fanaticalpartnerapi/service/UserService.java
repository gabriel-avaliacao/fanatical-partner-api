package com.example.fanaticalpartnerapi.service;

import com.example.fanaticalpartnerapi.controller.request.UserRequest;
import com.example.fanaticalpartnerapi.controller.response.UserResponse;
import com.example.fanaticalpartnerapi.dto.CampaignDTO;

import java.util.List;

public interface UserService {
    UserResponse saveUser(UserRequest userRequest);

    List<CampaignDTO> checkUser(String email);
}
