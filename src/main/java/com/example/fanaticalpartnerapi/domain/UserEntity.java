package com.example.fanaticalpartnerapi.domain;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "user")
public class UserEntity {

    @Id
    @Column(name = "idt_email", nullable = false)
    private String email;

    @Column(name = "des_name")
    private String name;

    @Column(name = "dat_birthday")
    private LocalDate birthday;

    @ManyToOne
    @JoinColumn(name = "idt_team", referencedColumnName = "idt_team")
    private TeamEntity teamEntity;
}
