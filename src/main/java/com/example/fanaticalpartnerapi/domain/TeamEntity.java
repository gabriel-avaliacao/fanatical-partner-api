package com.example.fanaticalpartnerapi.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "team")
public class TeamEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idt_team")
    private Long id;

    @Column(name = "des_name", nullable = false)
    private String name;
}
