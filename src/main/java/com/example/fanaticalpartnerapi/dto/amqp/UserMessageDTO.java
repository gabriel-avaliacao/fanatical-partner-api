package com.example.fanaticalpartnerapi.dto.amqp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserMessageDTO implements Serializable {

    private String email;
    private Long teamId;
    private long count;

    public boolean toRetry(long maxRetry){
        return count >= maxRetry;
    }

    public void plusCount() {
        ++count;
    }
}
