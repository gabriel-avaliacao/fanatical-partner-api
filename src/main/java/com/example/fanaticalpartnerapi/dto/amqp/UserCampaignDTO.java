package com.example.fanaticalpartnerapi.dto.amqp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserCampaignDTO {

    private String email;
    private Long campaignId;
    private long count;

    public boolean toRetry(long maxRetry){
        return count >= maxRetry;
    }

    public void plusCount() {
        ++count;
    }
}
