package com.example.fanaticalpartnerapi.dto;

import lombok.Data;

@Data
public class CampaignDTO {

    private Long id;
    private String name;
}
