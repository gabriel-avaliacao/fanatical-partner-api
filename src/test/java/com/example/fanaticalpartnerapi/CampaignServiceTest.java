package com.example.fanaticalpartnerapi;

import com.example.fanaticalpartnerapi.controller.response.DefaultErrorResponse;
import com.example.fanaticalpartnerapi.dto.CampaignDTO;
import com.example.fanaticalpartnerapi.dto.amqp.UserCampaignDTO;
import com.example.fanaticalpartnerapi.exception.FanaticalPartnerException;
import com.example.fanaticalpartnerapi.service.CampaignClient;
import com.example.fanaticalpartnerapi.service.impl.CampaignServiceImpl;
import com.example.fanaticalpartnerapi.utils.MessageDefinitionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
class CampaignServiceTest {

    public static final Long TEAM_ID = 1L;
    public static final Long CAMPAIGN_ID = 1L;
    public static final String CAMPAIGN_NAME = "Vai timão";
    public static final String USER_EMAIL = "gabrielmbdals@gmail.com";

    @InjectMocks
    private CampaignServiceImpl campaignService;

    @Mock
    private CampaignClient campaignClient;

    @Test
    void testFindCampaignActivesByTeamSuccess(){
        CampaignDTO[] campaignDTOArray = mockCampaignArrayDTO();
        when(campaignClient.findCampaignByTeam(TEAM_ID)).thenReturn(campaignDTOArray);
        List<CampaignDTO> campaignDTOS = campaignService.findCampaignActivesByTeam(TEAM_ID);

        assertEquals(1,campaignDTOS.size());
        assertEquals(CAMPAIGN_ID,campaignDTOS.get(0).getId());
        assertEquals(CAMPAIGN_NAME,campaignDTOS.get(0).getName());
    }

    @Test
    void testFindCampaignActivesByTeamNotFoundCampaigns(){
        when(campaignClient.findCampaignByTeam(TEAM_ID)).thenReturn(new CampaignDTO[]{});
        List<CampaignDTO> campaignDTOS = campaignService.findCampaignActivesByTeam(TEAM_ID);
        assertEquals(0,campaignDTOS.size());
    }

    @Test
    void testFindCampaignActivesByTeamErrorServiceCampaignApi(){
        DefaultErrorResponse expectedError = MessageDefinitionUtils.ERROR_CAMPAIGN_API_UNKNOW.getDefaultErrorResponse();
        when(campaignClient.findCampaignByTeam(TEAM_ID)).thenThrow(new RuntimeException());
        DefaultErrorResponse errorResponse = Assertions.assertThrows(FanaticalPartnerException.class,
                () -> campaignService.findCampaignActivesByTeam(TEAM_ID))
                .getMessageDefinition().getDefaultErrorResponse();
        assertEquals(expectedError.getCode(),errorResponse.getCode());
        assertEquals(expectedError.getDescription(),errorResponse.getDescription());
    }

    @Test
    void testRelateUserCampaignSuccess(){
        UserCampaignDTO userCampaignDTO = mockUserCampaignDTO();
        when(campaignClient.relateUserCampaign(userCampaignDTO.getCampaignId(),userCampaignDTO.getEmail()))
                .thenReturn(mockResponseEntity());
        campaignService.relateUserCampaign(userCampaignDTO);
    }

    @Test
    void testRelateUserCampaignIsNot2x(){
        DefaultErrorResponse expectedError = MessageDefinitionUtils.ERROR_CAMPAIGN_API_NOT_FOUND_ACTIVES
                .getDefaultErrorResponse();
        UserCampaignDTO userCampaignDTO = mockUserCampaignDTO();
        when(campaignClient.relateUserCampaign(userCampaignDTO.getCampaignId(),userCampaignDTO.getEmail()))
                .thenReturn(new ResponseEntity(HttpStatus.BAD_REQUEST));
        DefaultErrorResponse errorResponse = Assertions.assertThrows(FanaticalPartnerException.class,
                () -> campaignService.relateUserCampaign(userCampaignDTO))
                .getMessageDefinition().getDefaultErrorResponse();
        assertEquals(expectedError.getCode(),errorResponse.getCode());
        assertEquals(expectedError.getDescription(),errorResponse.getDescription());
    }

    @Test
    void testFindCampaignByUserWithoutCampaigns(){
        when(campaignClient.findCampaignByUser(USER_EMAIL)).thenReturn(new CampaignDTO[]{});
        List<CampaignDTO> campaignByUser = campaignService.findCampaignByUser(USER_EMAIL);
        assertEquals(0, campaignByUser.size());
    }

    @Test
    void testFindCampaignByUserWithCampaigns(){
        when(campaignClient.findCampaignByUser(USER_EMAIL)).thenReturn(mockCampaignArrayDTO());
        List<CampaignDTO> campaignByUser = campaignService.findCampaignByUser(USER_EMAIL);
        assertEquals(1, campaignByUser.size());
        assertEquals(CAMPAIGN_NAME, campaignByUser.get(0).getName());
        assertEquals(CAMPAIGN_ID, campaignByUser.get(0).getId());
    }

    private ResponseEntity<String> mockResponseEntity() {
        return new ResponseEntity(HttpStatus.OK);
    }

    private CampaignDTO[] mockCampaignArrayDTO() {
        return new CampaignDTO[]{mockCampaignDTO()};
    }

    private CampaignDTO mockCampaignDTO() {
        CampaignDTO campaignDTO = new CampaignDTO();
        campaignDTO.setId(CAMPAIGN_ID);
        campaignDTO.setName(CAMPAIGN_NAME);
        return campaignDTO;
    }

    private UserCampaignDTO mockUserCampaignDTO(){
        return UserCampaignDTO.builder()
                .email(USER_EMAIL)
                .campaignId(CAMPAIGN_ID)
                .build();
    }
}
