package com.example.fanaticalpartnerapi;

import com.example.fanaticalpartnerapi.controller.request.TeamRequest;
import com.example.fanaticalpartnerapi.controller.response.DefaultErrorResponse;
import com.example.fanaticalpartnerapi.controller.response.TeamResponse;
import com.example.fanaticalpartnerapi.domain.TeamEntity;
import com.example.fanaticalpartnerapi.exception.FanaticalPartnerException;
import com.example.fanaticalpartnerapi.repository.TeamRepository;
import com.example.fanaticalpartnerapi.repository.UserRepository;
import com.example.fanaticalpartnerapi.service.impl.TeamServiceImpl;
import com.example.fanaticalpartnerapi.utils.MessageDefinitionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@SpringBootTest
class TeamServiceTest {

    public static final String TEAM_NAME = "Timão";
    public static final long TEAM_ID = 1L;

    @InjectMocks
    private TeamServiceImpl teamService;

    @Mock
    private TeamRepository teamRepository;

    @Mock
    private UserRepository userRepository;

    @Test
    void testSaveSuccess(){
        TeamEntity teamEntity = mockTeamEntity();
        when(teamRepository.save(any())).thenReturn(teamEntity);
        TeamResponse response = teamService.save(mockTeamRequest());

        assertEquals(teamEntity.getId(), response.getId());
        assertEquals(teamEntity.getName(), response.getName());
    }

    @Test
    void testDeleteSuccess(){
        TeamEntity teamEntity = mockTeamEntity();
        when(teamRepository.findById(TEAM_ID)).thenReturn(Optional.of(teamEntity));
        when(userRepository.countByTeamEntityId(TEAM_ID)).thenReturn(0L);
        doNothing().when(teamRepository).deleteById(TEAM_ID);
        teamService.delete(TEAM_ID);
    }

    @Test
    void testDeleteEntityNotFound(){
        DefaultErrorResponse expectedErrorResponse =
                MessageDefinitionUtils.ERROR_TEAM_NOT_FOUND.getDefaultErrorResponse();

        when(teamRepository.findById(TEAM_ID)).thenReturn(Optional.empty());
        DefaultErrorResponse errorResponse = Assertions.assertThrows(FanaticalPartnerException.class,
                () -> teamService.delete(TEAM_ID))
                .getMessageDefinition().getDefaultErrorResponse();

        assertEquals(expectedErrorResponse.getCode(), errorResponse.getCode());
        assertEquals(expectedErrorResponse.getDescription(), errorResponse.getDescription());
    }

    @Test
    void testDeleteUserLinkWithTeamError(){
        DefaultErrorResponse expectedErrorResponse =
                MessageDefinitionUtils.ERROR_TEAM_CANNOT_BE_DELETED.getDefaultErrorResponse();
        TeamEntity teamEntity = mockTeamEntity();
        when(teamRepository.findById(TEAM_ID)).thenReturn(Optional.of(teamEntity));
        when(userRepository.countByTeamEntityId(TEAM_ID)).thenReturn(1L);

        DefaultErrorResponse errorResponse = Assertions.assertThrows(FanaticalPartnerException.class,
                () -> teamService.delete(TEAM_ID))
                .getMessageDefinition().getDefaultErrorResponse();

        assertEquals(expectedErrorResponse.getCode(), errorResponse.getCode());
        assertEquals(expectedErrorResponse.getDescription(), errorResponse.getDescription());
    }

    @Test
    void testFindAll(){
        List<TeamEntity> teamEntities = Arrays.asList(mockTeamEntity());
        when(teamRepository.findAll()).thenReturn(teamEntities);
        List<TeamResponse> teamResponses = teamService.findsAll();
        assertEquals(teamEntities.size(), teamResponses.size());
        assertEquals(teamEntities.get(0).getId(), teamResponses.get(0).getId());
        assertEquals(teamEntities.get(0).getName(), teamResponses.get(0).getName());
    }

    private TeamEntity mockTeamEntity() {
        TeamEntity teamEntity = new TeamEntity();
        teamEntity.setName(TEAM_NAME);
        teamEntity.setId(TEAM_ID);
        return teamEntity;
    }

    private TeamRequest mockTeamRequest() {
        TeamRequest teamRequest = new TeamRequest();
        teamRequest.setName(TEAM_NAME);
        return teamRequest;
    }

}
