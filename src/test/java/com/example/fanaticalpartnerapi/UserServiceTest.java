package com.example.fanaticalpartnerapi;

import com.example.fanaticalpartnerapi.amqp.publisher.NewUserPublisher;
import com.example.fanaticalpartnerapi.controller.request.UserRequest;
import com.example.fanaticalpartnerapi.controller.response.DefaultErrorResponse;
import com.example.fanaticalpartnerapi.controller.response.TeamResponse;
import com.example.fanaticalpartnerapi.controller.response.UserResponse;
import com.example.fanaticalpartnerapi.domain.TeamEntity;
import com.example.fanaticalpartnerapi.domain.UserEntity;
import com.example.fanaticalpartnerapi.dto.CampaignDTO;
import com.example.fanaticalpartnerapi.dto.amqp.UserCampaignDTO;
import com.example.fanaticalpartnerapi.exception.FanaticalPartnerException;
import com.example.fanaticalpartnerapi.exception.ValidateDateException;
import com.example.fanaticalpartnerapi.repository.UserRepository;
import com.example.fanaticalpartnerapi.service.CampaignService;
import com.example.fanaticalpartnerapi.service.TeamService;
import com.example.fanaticalpartnerapi.service.impl.UserServiceImpl;
import com.example.fanaticalpartnerapi.utils.MessageDefinitionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.example.fanaticalpartnerapi.utils.ValidatePeriodUtils.DATE_FORMAT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@SpringBootTest
class UserServiceTest {

    public static final String USER_NAME = "Gabriel Morais";
    public static final String USER_EMAIL = "gabrielmbdals@gmail.com";
    public static final String USER_BIRTHDAY = "14/12/1989";
    public static final long TEAM_ID = 1L;
    private static final String TEAM_NAME = "Timão do Coração";
    public static final long CAMPAIGN_ID = 1L;

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private TeamService teamService;

    @Mock
    private NewUserPublisher newUserPublisher;

    @Mock
    private CampaignService campaignService;

    @Test
    void testCreateUserSuccess(){
        UserRequest userRequest = mockUserRequest();
        when(userRepository.findById(USER_EMAIL)).thenReturn(Optional.empty());
        when(userRepository.save(any())).thenReturn(mockUserEntity());
        doNothing().when(newUserPublisher).publishNewUser(mockUserEntity());
        when(teamService.convertToResponse(mockTeamEntity())).thenReturn(mockTeamResponse());
        UserResponse userResponse = userService.saveUser(userRequest);
        assertEquals(userRequest.getEmail(), userResponse.getEmail());
        assertEquals(userRequest.getName(), userResponse.getName());
        assertEquals(userRequest.getBirthday(), userResponse.getBirthday().format(DATE_FORMAT));
        assertEquals(userRequest.getTeamId(), userResponse.getTeam().getId());
    }

    @Test
    void testCheckUserNotFound(){
        DefaultErrorResponse expectedError = MessageDefinitionUtils.ERROR_USER_NOT_FOUND.getDefaultErrorResponse();

        when(userRepository.findById(USER_EMAIL)).thenReturn(Optional.empty());
        when(campaignService.findCampaignByUser(USER_EMAIL)).thenReturn(mockCampaignDTOList());

        DefaultErrorResponse errorResponse = Assertions.assertThrows(FanaticalPartnerException.class,
                () -> userService.checkUser(USER_EMAIL))
                .getMessageDefinition().getDefaultErrorResponse();

        assertEquals(expectedError.getDescription(), errorResponse.getDescription());
        assertEquals(expectedError.getCode(), errorResponse.getCode());
    }

    @Test
    void testCheckUserWithCampaign(){
        DefaultErrorResponse expectedError = MessageDefinitionUtils.ERROR_EMAIL_ALREADY_REGISTERED
                .getDefaultErrorResponse();
        when(userRepository.findById(USER_EMAIL)).thenReturn(Optional.of(mockUserEntity()));
        when(campaignService.findCampaignByUser(USER_EMAIL)).thenReturn(mockCampaignDTOList());
        DefaultErrorResponse errorResponse = Assertions.assertThrows(FanaticalPartnerException.class,
                () -> userService.checkUser(USER_EMAIL))
                .getMessageDefinition().getDefaultErrorResponse();
        assertEquals(expectedError.getDescription(), errorResponse.getDescription());
        assertEquals(expectedError.getCode(), errorResponse.getCode());
    }

    @Test
    void testCheckUserWithNewCampaign(){
        when(userRepository.findById(USER_EMAIL)).thenReturn(Optional.of(mockUserEntity()));
        when(campaignService.findCampaignByUser(USER_EMAIL)).thenReturn(Collections.emptyList());
        when(campaignService.findCampaignActivesByTeam(TEAM_ID)).thenReturn(mockCampaignDTOList());
        doNothing().when(newUserPublisher).publishRelateUserCampaign(mockUserCampaignDTO());
        List<CampaignDTO> campaignDTOS = userService.checkUser(USER_EMAIL);
        assertNotNull(campaignDTOS);
    }

    @Test
    void testCreateUserExistsError(){
        DefaultErrorResponse expectedError = MessageDefinitionUtils.ERROR_EMAIL_ALREADY_REGISTERED.getDefaultErrorResponse();
        when(userRepository.findById(USER_EMAIL)).thenReturn(Optional.of(mockUserEntity()));
        DefaultErrorResponse errorResponse = Assertions.assertThrows(FanaticalPartnerException.class,
                () -> userService.saveUser(mockUserRequest())).getMessageDefinition().getDefaultErrorResponse();
        assertEquals(expectedError.getDescription(), errorResponse.getDescription());
        assertEquals(expectedError.getCode(), errorResponse.getCode());
    }

    @Test
    void testValidatePeriodError(){
        DefaultErrorResponse expectedError = MessageDefinitionUtils.ERROR_DATE_VALIDATE.getDefaultErrorResponse();
        UserRequest userRequest = mockUserRequest();
        userRequest.setBirthday("20/20/2020");
        when(userRepository.findById(USER_EMAIL)).thenReturn(Optional.empty());
        DefaultErrorResponse errorResponse = Assertions.assertThrows(ValidateDateException.class, () -> userService.saveUser(userRequest))
                .getMessageDefinition().getDefaultErrorResponse();
        assertEquals(expectedError.getDescription(), errorResponse.getDescription());
        assertEquals(expectedError.getCode(), errorResponse.getCode());
    }


    private UserRequest mockUserRequest(){
        UserRequest request = new UserRequest();
        request.setName(USER_NAME);
        request.setEmail(USER_EMAIL);
        request.setBirthday(USER_BIRTHDAY);
        request.setTeamId(TEAM_ID);
        return request;
    }

    private UserEntity mockUserEntity(){
        UserEntity userEntity = new UserEntity();
        userEntity.setName(USER_NAME);
        userEntity.setEmail(USER_EMAIL);
        userEntity.setBirthday(LocalDate.parse(USER_BIRTHDAY, DATE_FORMAT));
        userEntity.setTeamEntity(mockTeamEntity());
        return userEntity;
    }

    private TeamEntity mockTeamEntity() {
        TeamEntity teamEntity = new TeamEntity();
        teamEntity.setName(TEAM_NAME);
        teamEntity.setId(TEAM_ID);
        return teamEntity;
    }

    private List<CampaignDTO> mockCampaignDTOList() {
        List<CampaignDTO> campaignDTOS = Arrays.asList(mockCampaignDTO());
        return campaignDTOS;
    }

    private CampaignDTO mockCampaignDTO() {
        CampaignDTO campaignDTO = new CampaignDTO();
        campaignDTO.setId(CAMPAIGN_ID);
        campaignDTO.setName("Ti ti ti timão em dobro");
        return campaignDTO;
    }

    private UserCampaignDTO mockUserCampaignDTO(){
        return UserCampaignDTO.builder()
                .email(USER_EMAIL)
                .campaignId(CAMPAIGN_ID)
                .build();
    }

    private TeamResponse mockTeamResponse(){
        return TeamResponse.builder().name(TEAM_NAME).id(TEAM_ID).build();
    }
}
