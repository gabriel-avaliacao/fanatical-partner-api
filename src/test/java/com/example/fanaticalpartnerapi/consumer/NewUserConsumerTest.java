package com.example.fanaticalpartnerapi.consumer;

import com.example.fanaticalpartnerapi.amqp.RabbitmqPropertiesConfig;
import com.example.fanaticalpartnerapi.amqp.consumer.NewUserConsumer;
import com.example.fanaticalpartnerapi.amqp.publisher.NewUserPublisher;
import com.example.fanaticalpartnerapi.dto.CampaignDTO;
import com.example.fanaticalpartnerapi.dto.amqp.UserCampaignDTO;
import com.example.fanaticalpartnerapi.dto.amqp.UserMessageDTO;
import com.example.fanaticalpartnerapi.service.CampaignService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@SpringBootTest
class NewUserConsumerTest {

    public static final long TEAM_ID = 1L;
    public static final long CAMPAIGN_ID = 1L;
    public static final String CAMPAIGN_NAME = "Vai timão";
    public static final String USER_EMAIL = "gabrielmbdals@gmail.com";
    public static final long QUEUE_NEW_USER_MAX_RETRY = 5L;

    @InjectMocks
    private NewUserConsumer newUserConsumer;

    @Mock
    private CampaignService campaignService;

    @Mock
    private RabbitmqPropertiesConfig fanaticalProperties;

    @Mock
    private NewUserPublisher newUserPublisher;

    @Test
    void testConsumerNewUserSuccess(){
        when(fanaticalProperties.getQueueNewUserMaxRetry()).thenReturn(QUEUE_NEW_USER_MAX_RETRY);
        doNothing().when(newUserPublisher).publishRelateUserCampaign(mockUserCampaignDTO());
        when(campaignService.findCampaignActivesByTeam(TEAM_ID)).thenReturn(mockCampaignDTOList());
        newUserConsumer.consumerNewUser(mockUserMessageDTO());
    }

    @Test
    void testConsumerNewUserMaxRetryError(){
        UserMessageDTO userMessageDTO = mockUserMessageDTO();
        userMessageDTO.setCount(QUEUE_NEW_USER_MAX_RETRY);
        when(fanaticalProperties.getQueueNewUserMaxRetry()).thenReturn(QUEUE_NEW_USER_MAX_RETRY);
        Assertions.assertThrows(AmqpRejectAndDontRequeueException.class,
                () -> newUserConsumer.consumerNewUser(userMessageDTO));
    }

    private UserCampaignDTO mockUserCampaignDTO() {
        return UserCampaignDTO.builder()
                .build();
    }

    private List<CampaignDTO> mockCampaignDTOList() {
        return Arrays.asList(mockCampaignDTO());
    }

    private CampaignDTO mockCampaignDTO() {
        CampaignDTO campaignDTO = new CampaignDTO();
        campaignDTO.setName(CAMPAIGN_NAME);
        campaignDTO.setId(CAMPAIGN_ID);
        return campaignDTO;
    }

    private UserMessageDTO mockUserMessageDTO(){
        return UserMessageDTO.builder()
                .teamId(TEAM_ID)
                .email(USER_EMAIL)
                .build();
    }

}
