package com.example.fanaticalpartnerapi.consumer;

import com.example.fanaticalpartnerapi.amqp.RabbitmqPropertiesConfig;
import com.example.fanaticalpartnerapi.amqp.consumer.RelateUserCampaignConsumer;
import com.example.fanaticalpartnerapi.amqp.publisher.GenericPublisher;
import com.example.fanaticalpartnerapi.dto.amqp.UserCampaignDTO;
import com.example.fanaticalpartnerapi.exception.FanaticalPartnerException;
import com.example.fanaticalpartnerapi.service.CampaignService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@SpringBootTest
class RelateUserCampaignConsumerTest {


    public static final long QUEUE_RELATE_USER_CAMPAIGN_MAX_RETRY = 5L;
    public static final String EXCHANGE_RELATE_USER_CAMPAIGN = "exchange_relate_user_campaign";
    public static final String ROUTING_KEY_RELATE_USER_CAMPAIGN = "routing.key.relate.user.campaign";

    @InjectMocks
    private RelateUserCampaignConsumer relateUserCampaignConsumer;

    @Mock
    private CampaignService campaignService;

    @Mock
    private GenericPublisher genericPublisher;

    @Mock
    private RabbitmqPropertiesConfig fanaticalProperties;

    @Test
    void testConsumerRelateUserCampaign(){
        UserCampaignDTO userCampaignDTO = mockUserCampaignDTO();
        when(fanaticalProperties.getQueueRelateUserCampaignMaxRetry())
                .thenReturn(QUEUE_RELATE_USER_CAMPAIGN_MAX_RETRY);
        doNothing().when(campaignService).relateUserCampaign(userCampaignDTO);
        relateUserCampaignConsumer.consumerRelateUserCampaign(userCampaignDTO);
    }

    @Test
    void testConsumerRelateUserCampaignMaxRetryError(){
        UserCampaignDTO userCampaignDTO = mockUserCampaignDTO();
        userCampaignDTO.setCount(QUEUE_RELATE_USER_CAMPAIGN_MAX_RETRY);
        when(fanaticalProperties.getQueueRelateUserCampaignMaxRetry())
                .thenReturn(QUEUE_RELATE_USER_CAMPAIGN_MAX_RETRY);
        Assertions.assertThrows(AmqpRejectAndDontRequeueException.class,
                () -> relateUserCampaignConsumer.consumerRelateUserCampaign(userCampaignDTO));
    }

    @Test
    void testConsumerRelateUserCampaignSendToRetry(){
        UserCampaignDTO userCampaignDTO = mockUserCampaignDTO();
        when(fanaticalProperties.getQueueRelateUserCampaignMaxRetry())
                .thenThrow(new FanaticalPartnerException());
        doNothing().when(genericPublisher).publishRetry(userCampaignDTO,EXCHANGE_RELATE_USER_CAMPAIGN,
                ROUTING_KEY_RELATE_USER_CAMPAIGN);
        relateUserCampaignConsumer.consumerRelateUserCampaign(userCampaignDTO);
    }

    private UserCampaignDTO mockUserCampaignDTO() {
        return UserCampaignDTO.builder()
                .build();
    }
}
