package com.example.fanaticalpartnerapi.publisher;

import com.example.fanaticalpartnerapi.amqp.publisher.GenericPublisher;
import com.example.fanaticalpartnerapi.dto.amqp.UserMessageDTO;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.Mockito.doNothing;

@SpringBootTest
class GenericPublisherTest {

    private static final String EXCHANGE_X = "exchange-x";
    private static final String ROUTING_KEY_X = "routing-key-x";
    @InjectMocks
    private GenericPublisher genericPublisher;

    @Mock
    private RabbitTemplate rabbitTemplate;

    @Test
    void testPublishRetrySuccess(){
        doNothing().when(rabbitTemplate).convertAndSend(EXCHANGE_X, ROUTING_KEY_X, mockUserMessageDTO());
        genericPublisher.publishRetry(mockUserMessageDTO(),EXCHANGE_X,ROUTING_KEY_X);
    }

    private UserMessageDTO mockUserMessageDTO(){
        return new UserMessageDTO();
    }
}
