package com.example.fanaticalpartnerapi.publisher;

import com.example.fanaticalpartnerapi.amqp.RabbitmqPropertiesConfig;
import com.example.fanaticalpartnerapi.amqp.publisher.NewUserPublisher;
import com.example.fanaticalpartnerapi.domain.TeamEntity;
import com.example.fanaticalpartnerapi.domain.UserEntity;
import com.example.fanaticalpartnerapi.dto.amqp.UserCampaignDTO;
import com.example.fanaticalpartnerapi.dto.amqp.UserMessageDTO;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static com.example.fanaticalpartnerapi.utils.ValidatePeriodUtils.DATE_FORMAT;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@SpringBootTest
class NewUserPublisherTest {

    public static final String USER_NAME = "Gabriel Morais";
    public static final String USER_EMAIL = "gabrielmbdals@gmail.com";
    public static final String USER_BIRTHDAY = "14/12/1989";
    public static final long CAMPAIGN_ID = 1L;
    public static final long TEAM_ID = 1L;
    private static final String TEAM_NAME = "Timão do Coração";
    public static final String EXCHANGE_NEW_USER = "exchange_new_user";
    public static final String ROUTING_KEY_NEW_USER = "routing.key.new.user";
    public static final String EXCHANGE_RELATE_USER_CAMPAIGN = "exchange_relate_user_campaign";
    public static final String ROUTING_KEY_RELATE_USER_CAMPAIGN = "routing.key.relate.user.campaign";

    @InjectMocks
    private NewUserPublisher newUserPublisher;

    @Mock
    private RabbitTemplate rabbitTemplate;

    @Mock
    private RabbitmqPropertiesConfig fanaticalProperties;

    @Test
    void testPublishNewUserSuccess(){
        when(fanaticalProperties.getExchangeNewUser()).thenReturn(EXCHANGE_NEW_USER);
        when(fanaticalProperties.getRoutingKeyNewUser()).thenReturn(ROUTING_KEY_NEW_USER);
        doNothing().when(rabbitTemplate).convertAndSend(EXCHANGE_NEW_USER, ROUTING_KEY_NEW_USER,
                mockUserMessageDTO());
        newUserPublisher.publishNewUser(mockUserEntity());
    }

    @Test
    void testPublishRelateUserCampaignSuccess(){

        when(fanaticalProperties.getExchangeRelateUserCampaign()).thenReturn(EXCHANGE_RELATE_USER_CAMPAIGN);
        when(fanaticalProperties.getRoutingKeyRelateUserCampaign()).thenReturn(ROUTING_KEY_RELATE_USER_CAMPAIGN);
        doNothing().when(rabbitTemplate).convertAndSend(EXCHANGE_NEW_USER, ROUTING_KEY_NEW_USER, mockUserCampaignDTO());

        newUserPublisher.publishNewUser(mockUserEntity());
    }

    private UserEntity mockUserEntity(){
        UserEntity userEntity = new UserEntity();
        userEntity.setName(USER_NAME);
        userEntity.setEmail(USER_EMAIL);
        userEntity.setBirthday(LocalDate.parse(USER_BIRTHDAY, DATE_FORMAT));
        userEntity.setTeamEntity(mockTeamEntity());
        return userEntity;
    }

    private TeamEntity mockTeamEntity() {
        TeamEntity teamEntity = new TeamEntity();
        teamEntity.setName(TEAM_NAME);
        teamEntity.setId(TEAM_ID);
        return teamEntity;
    }

    private UserMessageDTO mockUserMessageDTO(){
        return UserMessageDTO.builder()
                .email(USER_EMAIL)
                .teamId(TEAM_ID).build();
    }

    private UserCampaignDTO mockUserCampaignDTO(){
        return UserCampaignDTO.builder()
                .campaignId(CAMPAIGN_ID)
                .email(USER_EMAIL).build();
    }

}
