**Fanatical Partner API**

API Sócio Torcedor

---
O sistema possui as seguintes regras implementadas:

* Dado um E-mail que já existe, informar que o cadastro já foi efetuado, porém, caso o cliente
  não tenha nenhuma campanha associada, o serviço deverá enviar as novas campanhas como
  resposta;
  
* O Cadastro deve ser composto de:
 
> * Nome Completo; <br>
> * E-mail;<br>
> * Data de Nascimento;<br>
> * Meu Time do Coração; <br>

* O Cliente não pode ter mais de um cadastro ativo;
* Ao efetuar o cadastro do usuário, utilize os serviços criados na API **Campaign API** para efetuar as
  operações e as associações necessárias:
 
``` 
o   O Cadastramento do cliente ocorre antes da associação com as campanhas, ou seja, 
    o processo de cadastro e associação ocorre em dois momentos separados;
o   O Usuários pode ter N Campanhas associadas a ele; Lembrando que as campanhas 
    são associadas ao Time do Coração;
o   A associação do usuário as campanhas podem ocorrer em dois momentos:
        ▪ Se for usuário novo: Após o cadastramento do usuário, o sistema deverá solicitar as 
          campanhas ativas para aquele time do coração e efetuar a associação;
        ▪ Se for um usuário já cadastrado: Deverá ser feita a associação das campanhas
          novas, ou seja, as que o usuário daquele time do coração não tem
          relacionamento até o momento.
o   O Consumo das listas das campanhas deve ser feita via Serviço exposto conforme descrito no exercício anterior;
o   O Cadastramento das campanhas deverá ser feito via Serviço (API, conforme descrito no exercício anterior)
o   O Cadastramento não pode ser influenciado pelo serviço das campanhas, caso o serviço das campanhas não esteja “UP”;
```
---

## Requisitos 
• [Java 1.8](https://www.oracle.com/br/java/technologies/javase/javase-jdk8-downloads.html) 
<br> • [Docker Componse](https://docs.docker.com/compose/install/#prerequisites)
<br>

---
## Rodando o ambiente local

1. Rode o banco Mysql em docker usando o camando **docker-compose up -d**
2. Execute o migration através do flyway para criar a estrutura na base usando o comando **./gradlew flywayMigrate**
3. Execute a aplicação informando o profile local **-Dspring.profiles.active=local** 
 
---
## Documentação do serviço

* Com a aplicação UP basta acessar o endereço http://localhost:8081/swagger-ui.html para acessar
a documentação dos serviços disponibilizados.

---

## Tecnologias utilizadas

* Spring Boot
* Spring Web
* Spring Data JPA
* Spring Cloud Sleuth
* Swagger
* Lombok
* H2 Database
* Flyway
* Mysql in Docker
* Netflix Eureka Client
* OpenFeign

---

## Este serviço em conjunto com mais 2 contempla o projeto como um todo 

#### 1 - Fanatical Partner API - https://bitbucket.org/gabriel-avaliacao/fanatical-partner-api.git
#### 2 - Campaign API - https://bitbucket.org/gabriel-avaliacao/campaign-api.git
#### 3 - Service Discovery API - https://bitbucket.org/gabriel-avaliacao/service-discovery-api.git

---

#### Observações:
Para o perfeito funcionamento da API, verifique:

1 - Banco de dados esteja UP. **docker-compose up -d**
<br>
2 - Subir serviço **Service Discovery API**
<br>
3 - Serviço do **Rabbitmq** esteja UP: A API Service Discovery possui um docker-compose, subir ele também.
<br>
4 - Subir o serviço **Campaign API**   

---
