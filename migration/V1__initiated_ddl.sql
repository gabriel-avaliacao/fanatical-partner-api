CREATE TABLE team (
	idt_team BIGINT NOT NULL AUTO_INCREMENT,
	des_name VARCHAR(150) NOT NULL,
	PRIMARY KEY (idt_team)
);

CREATE TABLE user (
	idt_email VARCHAR(150) NOT NULL,
	idt_team BIGINT NOT NULL,
	des_name VARCHAR(150) NOT NULL,
	dat_birthday DATE NOT NULL,
	PRIMARY KEY (idt_email),
	FOREIGN KEY (idt_team) REFERENCES team (idt_team)
);
